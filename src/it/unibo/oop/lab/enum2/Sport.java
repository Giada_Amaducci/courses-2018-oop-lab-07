/**
 * 
 */
package it.unibo.oop.lab.enum2;
import static it.unibo.oop.lab.enum2.Place.*;

/**
 * Represents an enumeration for declaring sports.
 * 
 * 1) You must add a field keeping track of the number of members each team is
 * composed of (1 for individual sports)
 * 
 * 2) A second field will keep track of the name of the sport.
 * 
 * 3) A third field, of type Place, will allow to define if the sport is
 * practiced indoor or outdoor
 * 
 */
public enum Sport {

	BASKET(5, "Basket", INDOOR), VOLLEY(6, "Volley", INDOOR), TENNIS(1, "Tennis", OUTDOOR),
	BIKE(1, "Bike", OUTDOOR), F1(1, "F1", OUTDOOR), MOTOGP(1, "MotoGp", OUTDOOR), SOCCER(11,"Soccer",OUTDOOR);

	private final int member;
	private final String name;
	private final Place locate;

	private Sport(final int member, final String name, final Place locate) {
		this.member = member;
		this.name = name;
		this.locate = locate;
	}

	public boolean isIndividualSprot() {
		return member == 1;
	}
	
	public boolean isIndoorSport() {
		return (this.locate == INDOOR);
	}

	public Place getPlace() {
		return this.locate;
	}
	
	public String toString() {
		return this.name + ": located: " + this.getPlace() + " with team of " + this.member + "members";
	}
	/*
	 * TODO
	 * 
	 * [METHODS] To be defined
	 * 
	 * 
	 * 1) public boolean isIndividualSport()
	 * 
	 * Must return true only if called on individual sports
	 * 
	 * 
	 * 2) public boolean isIndoorSport()
	 * 
	 * Must return true in case the sport is practices indoor
	 * 
	 * 
	 * 3) public Place getPlace()
	 * 
	 * Must return the place where this sport is practiced
	 * 
	 * 
	 * 4) public String toString()
	 * 
	 * Returns the string representation of a sport
	 */
}
